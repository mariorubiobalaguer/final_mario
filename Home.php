<?php

namespace App\Controllers;
use Config\Services;
use App\Models\FamiliaModel;

class Home extends BaseController
{
    protected $auth;
    protected $session;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);

        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();

    }


    public function index()
    {
        if (!$this->auth->loggedIn()){
            return redirect()->to('auth/login');
        }
       
        return view('welcome_message');
    }
    #
    #public function catalogo() 
    #{
     #   $familia = new FamiliaModel();

      #  $data['familia'] = $familia->findAll();

       # print_r($data);
    #}
#
    public function catalogo() {
       $familias = new FamiliaModel();
       $titulo['titulo'] = "Catalogo";
       $data['familias'] = $familias->SELECT ("NombreFamilia ,CodigoFamilia")
                ->findAll();
        
       echo view ('articuloscatalogo',$data);
    }
    #public function borrar($codigoproducto){
     #   if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
      #  $ProductoModel = new ProductoModel();
       # $ProductoModel->delete($codigoproducto);
        #return redirect()->to('home');
        #} else {
        #    echo "No puedes borrarlo, pideselo a un administrador ";
        #}
        #ESto iria en la vista
        #<a href="<?= site_url('home/borrar/'.$codigoproducto['codigoproducto'])
    #class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar el producto?')">Borrar</a>?-> (no me deja comentarlo sin el guion" 
        
    #public function vista(){
    #if ($this->auth->loggedIn()){
    #return redirect()->to('auth/login');
    #}
    #return view('catalogo');
    
    #SIgue en el word, no me deja comentarlo bien


}
}


    
    